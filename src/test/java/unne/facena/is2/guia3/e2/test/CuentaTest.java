package unne.facena.is2.guia3.e2.test;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import unne.facena.is2.guia3.e2.Cuenta;
import java.util.Date;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Pablo N. Garcia Solanellas
 */
public class CuentaTest {
       
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }
    
    // Para correr TODOS los TEST [Alt + F6]
    // Para correr SOLO los TEST de esta clase [Ctrl + SHIFT + F6]    
    @Test
    public void testCta() {
        
        //Probamos la Validez de las CUENTAS.
        CuentaTest.constructorPorDefecto();
        CuentaTest.sinNumero();
        CuentaTest.sinFechaAlta();
        //END - Validez de la cuenta.
        
        //Probemos         
        CuentaTest.extraccionTest();
    }
    private static void extraccionTest(){
        
        Cuenta _cuenta = new Cuenta(4567, new Date(), 700);
        //Emprecion del saldo a modo de ejemplo 
        System.out.println("Saldo INICIAL:: " +_cuenta.getSaldo());
        System.out.println("Extraemos $400");
        //Intentamos retirar $400 como el saldo es 
        //$700 el saldo que nos queda en $300.        
        assertTrue(_cuenta.disminuirSaldo(400) );
        
        //-----------------------
        //Emprecion del saldo a modo de ejemplo 
        System.out.println("Saldo luego de la 1° extraccion " +_cuenta.getSaldo());
        System.out.println("Ahora intentemos extraer $400 mas");
        //Intentamos retiramos $400 mas. pero el saldo es de 300. 
        //No deberiamos poder retirar.
        assertTrue(_cuenta.disminuirSaldo(400) );      
    }
    /**
     * Pruebas del metodo puede extraer de la clase Cuenta
     */    
    private static void puedeExtraerTest(){
        
        //Instanciamos una cuenta para realizar la prueba del metodo Puede Extraer
        Cuenta _cuenta = new Cuenta(4567, new Date(), 16000);
        
        //podemos Extraer  cantidades negativas
           
        
        //podemos Extraer 0 pesos
        assertTrue(_cuenta.puedeEstraer(0));        
        //podemos Extraer 10 pesos
        assertTrue(_cuenta.puedeEstraer(10));
            //podemos Extraer 500 pesos
        assertTrue(_cuenta.puedeEstraer(500));        
        //podemos Extraer 999 pesos
        assertTrue(_cuenta.puedeEstraer(999));
        //podemos Extraer Mil pesos
        assertTrue(_cuenta.puedeEstraer(10000));
        
        //podemos Extraer 15Mil pesos
        assertTrue(_cuenta.puedeEstraer(15000));
        
         //supera el limite de extracion
       
        
        
    }
    
    /**
     * Probamos una instancia de la clase Cuenta creada por el constructor por defecto.
     */
    private static void constructorPorDefecto(){
        /*
        * Instanciamos una cuenta con el constructor por defecto.
        * -SIN Numero de cuenta. 
        * -SIN fecha de alta.        
         */
        Cuenta _cuenta = new Cuenta();
        
        /* Esperamos que el metodo isCuentaValida nos indique que esta no es valida.
        * ya que no cuenta ni con numero de cuenta ni con una fecha de alta.
        */        
        assertFalse(_cuenta.isCuentaValida());
    }
    
    /**
     * Probamos una instancia de la clase Cuenta que no tiene numero de cuenta
     */
    private static void sinNumero(){
        /* Instanciamos una cuenta INVALIDA. 
        * -SIN Numero de cuenta. 
        * -CON fecha de alta.
        */        
        Cuenta _cuentaInvalidaDos = new Cuenta(null,new Date(), 0, 0);
        
        //esperamos que el metodo isCuentaValida nos indique que esta cuenta
        // no es valida ya que no cuenta con un NUMERO de cuenta.
        assertFalse(_cuentaInvalidaDos.isCuentaValida());
    }
     
    /**
     * Probamos una instancia de la clase Cuenta que no tiene fecha de alta.
     */
    private static void sinFechaAlta(){
        /* Instanciamos una cuenta INVALIDA. 
        * -CON Numero de cuenta. 
        * -SIN fecha de alta.
        */        
        Cuenta _cuentaInvalidaTres = new Cuenta(456789,null, 0, 0);
        
        // esperamos que el metodo isCuentaValida nos indique que esta cuenta
        //no es valida ya que no cuenta con una fecha de alta.        
        assertFalse(_cuentaInvalidaTres.isCuentaValida());
    }
}
