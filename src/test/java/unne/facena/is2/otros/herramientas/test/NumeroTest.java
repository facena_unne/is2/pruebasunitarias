package unne.facena.is2.otros.herramientas.test;

import unne.facena.is2.otros.herramientas.Numero;
import static org.junit.Assert.assertEquals;  
import org.junit.Test; 

/**
 *
 * @author Pablo N. Garcia Solanellas FaCENA UNNE.
 * Catedra: Ingenieria del Software 2
 */
public class NumeroTest {
    Integer _esperado = null;
    
    
    // Para correr TODOS los TEST [Alt + F6]
    // Para correr SOLO los TEST de esta clase [Ctrl + SHIFT + F6]    
    @Test
    public void testCta() {
        //Caso de prueba encontrar el mas numero mas grande
        testFindMax();
        
    }
    
    public void testFindMax(){
        
        //probamos con un arreglo vacio
        assertEquals(null,Numero.elMasGrande(new int[]{}));        
        
        //Afirmamos que el número más alto es el entero más grande que el lenguaje puede generar.
        assertEquals(new Integer (Integer.MAX_VALUE),
            Numero.elMasGrande(new int[]{ 5,Integer.MAX_VALUE,2 })
        );
        
        //probamos que ocurre con un solo elemento en el array
        assertEquals((Integer)1,Numero.elMasGrande(new int[]{1}));        
        
        //probamos con algunos elementos al azar
        assertEquals((Integer)4,Numero.elMasGrande(new int[]{1,3,4,2}));  
        
        //Afirmamos que el menor de los numeros en el arreglo es -2.                
        assertEquals(new Integer(-2),Numero.elMasGrande(new int[]{-12,-3,-4,-2}));  
        
        //probamos con un números negativo
        //afirmamos que el menor de los numero es -5        
        assertEquals(new Integer(-5),Numero.elMasGrande(new int[]{-5}));        
        
        //probamos con el mínimo valor conocido        
        assertEquals(new Integer(Integer.MIN_VALUE),Numero.elMasGrande(new int[]{ Integer.MIN_VALUE }));
    }  
}
