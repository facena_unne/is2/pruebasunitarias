package unne.facena.is2.otros.herramientas.test;


import unne.facena.is2.otros.herramientas.Telefono;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import static org.junit.Assert.assertEquals;
import org.junit.Test;


/**
 *
 * @author Pablo N. Garcia Solanellas
 */
public class TelefonoTest {
    // TODO put test methods here.
    
    /*
    * *********** IMPORTANT*******************
    * En Netbeans alt+f6 para ejecutar el test.
    */

    @Test
    public void test() {
        //"^\\+(?:[0-9] ?){6,14}[0-9]$"
        
        Map<Telefono, Boolean> telefonos = new HashMap<Telefono,Boolean>();
        
        
        telefonos.put(new Telefono("0171 378 0647") ,true);        
        telefonos.put(new Telefono("(44.171)920 1007"),true);        
        telefonos.put( new Telefono("+44(0) 1225 753678"),true);        
        telefonos.put( new Telefono("01256 468551"),true);        
        telefonos.put( new Telefono("(96) 590-34-00"),false);        
        telefonos.put( new Telefono("96 590 3400"),false);        
        telefonos.put( new Telefono("1-845-225-3000"),true);        
        telefonos.put( new Telefono("212.995.5405"),false);        
        telefonos.put( new Telefono("+45 43 48 60 61"),true);        
        telefonos.put( new Telefono("95-51-279648"),false);        
        telefonos.put( new Telefono("+411/285 3797"),true);        
        telefonos.put( new Telefono("+49 69 136-2 98 05"),true);        
        telefonos.put( new Telefono("33 1 34 43 32 26"),true);        
        telefonos.put( new Telefono("++31-20-5200161"),true);        
        telefonos.put( new Telefono("+54 379-4465084"),true);        
        telefonos.put( new Telefono("+54 11 3514 3874"),true);        
                
        
        for(Map.Entry<Telefono,Boolean> _entry : telefonos.entrySet()){
            assertEquals (_entry.getValue(), _entry.getKey().esInternacional());            
            //System.err.println(_entry.getKey().getNroTelefono() + " BIEN "  + (_entry.getKey().esInternacional() == _entry.getValue()) );
        }
    }
    
    
    
}
