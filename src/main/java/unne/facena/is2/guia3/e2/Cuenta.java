package unne.facena.is2.guia3.e2;

import java.sql.Timestamp;
import java.util.Date;

/*
 * Esta clase modela las características y responsabilidades de las cuentas 
 * bancarias descriptas en el ejercicio 2 de la guía de trabajos  prácticos
 * numero 2 de  la materia Ingeniería de Software 2 de la 
 * Facultad de Ciencias Exactas y Naturales y Agrimensura de la 
 * Universidad nacional del nordeste. 
 *
 * @author Pablo N. Garcia Solanellas
 */
public class Cuenta {
    
    private Integer nroCuenta;
    private Date fechaAlta;
    private double saldo;    
    private double limiteDiarioExtraccion;
    private static final double LIMITE_POR_DEFECT=15000;
    
    /**
    * Este constructor instancia un objeto de la clase Cuenta con
     * los atributos por defecto. El objeto resultante de esta instanciación
     * no es una cuenta valida.
     */
    public Cuenta(){
        this.setNroCuenta(null);
        this.setFechaAlta(new Timestamp(System.currentTimeMillis()));
        this.setSaldo(0);
        this.setLimiteDiarioExtraccion(LIMITE_POR_DEFECT);
    }
    
    /**
     * Instancia un objeto con los atributos que recibe como parámetro.
     * @param p_idCuenta el id de la cuenta bancaria
     * @param p_fechaAlta fecha en la que se dio de alta la cuenta.     
     */    
    public Cuenta(Integer p_idCuenta, Date p_fechaAlta, double p_saldo)
    {
        this.setNroCuenta(p_idCuenta);
        this.setFechaAlta(p_fechaAlta);
        this.setSaldo(p_saldo);
        this.setLimiteDiarioExtraccion(LIMITE_POR_DEFECT);
    }
    
    /**
     * Instancia un objeto con los atributos que recibe como parámetro.
     * @param p_idCuenta el id de la cuenta bancaria
     * @param p_fechaAlta fecha en la que se dio de alta la cuenta.
     * @param p_saldo saldo de la cuenta
     * @param p_limiteExtraccion  limite de extracion diario.
     */    
    public Cuenta(Integer p_idCuenta, Date p_fechaAlta,
        double p_saldo, double p_limiteExtraccion)
    {        
        this.setNroCuenta(p_idCuenta);
        this.setFechaAlta(p_fechaAlta);
        this.setSaldo(p_saldo);
        this.setLimiteDiarioExtraccion(p_limiteExtraccion);
    }
   
    
    /**
     * Disminuye el saldo en la cantidad indicada por el parametro que recibe
     * @param p_importe valor a decrementar el saldo.
     */
    public boolean disminuirSaldo(double p_importe) throws NumberFormatException{
        if (p_importe<0){
            throw  new NumberFormatException("El importe a descontar del saldo no tiene el formato adecuado.");
        }
        if (this.puedeEstraer(p_importe)){
            this.setSaldo(this.getSaldo()-p_importe);                        
            return true;
        }else{
            return false;
        }
        
    }
    
    /**
     * Verifica si es posible extraer de la cuenta in determinado importe
     * @param p_importe contiene el valor cuya disponibilidad se desea conocer.
     * @return verdadero si es posible extraer el importe que recibe como parametro.
     */
    public boolean puedeEstraer(double p_importe)throws NumberFormatException{
        //verificamos que el importe sea un numero > a 0,
        //en caso contrario lanzamos una excepción.
        if (p_importe<0){
            throw  new NumberFormatException("El importe no tiene el formato adecuado.");
        }
        
        //controlamos el limie diario
        if(p_importe<=this.getLimiteDiarioExtraccion()){
            //confirmamos si el saldo es suficiente para realizar la extraccion.
            return this.getSaldo()>=p_importe;
        }else{            
            return false;
        }
        
    }
    
    /**
     * La cuenta es validad di tien un numero de cuenta y una fecha de alta.
     */
    public boolean isCuentaValida()
    {
        return this.getNroCuenta()!=null && this.getFechaAlta()!=null;        
    }
    

    public Date getFechaAlta() {
        return fechaAlta;
    }

    private void setFechaAlta(Date fechaAlta) {
        this.fechaAlta = fechaAlta;
    }

    public double getSaldo() {
        return saldo;
    }

    private void setSaldo(double saldo) {
        this.saldo = saldo;
    }

    public double getLimiteDiarioExtraccion() {
        return limiteDiarioExtraccion;
    }

    public void setLimiteDiarioExtraccion(double limiteDiarioExtraccion) {
        this.limiteDiarioExtraccion = limiteDiarioExtraccion;
    }

    public Integer getNroCuenta() {
        return nroCuenta;
    }

    public void setNroCuenta(Integer nroCuenta) {
        this.nroCuenta = nroCuenta;
    }

   
    
}
