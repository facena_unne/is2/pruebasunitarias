/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package unne.facena.is2.otros.herramientas;

import java.util.StringTokenizer;

/**
 *
 * @author Pablo N. Garcia Solanellas
 */
public class Numero {
    /**
     * Método que retorna el numero más grande dentro del 
     * arreglo que recibe como parámetro.
     * @param p_arreglo arreglo de numeros enteros
     * @return el numero entermo mas grande incluido en el parametro: p_arreglo.
     */
    public static Integer elMasGrande(int p_arreglo[]){  
        //realizamos una primara instanciación del valor máximo
        //-en caso de que el arreglo este vacio null
        //-en otro caso instanciamos el marcador con el mínimo posible   
     
        Integer _max = p_arreglo.length==0?null:Integer.MIN_VALUE;          
        
        //recorremos el arreglo para busca del máximo valor.
        for(int i=0;i<p_arreglo.length;i++){  
            //cuando encontramos un numero más grande que el que ya teníamos
            //actualizamos el marcador con ese valor.
            if(_max<p_arreglo[i]){                
                _max=p_arreglo[i];  
            }                  
        }  
        return _max;  
    }//Fin método: elMasGrande
    
    
    
}
