/*
 *Ingenieria del software 2 
 *UNNE 2019
 */
package unne.facena.is2.otros.herramientas;


public class Telefono {    
    private String nroTelefono;
    
//private String[] paises = new String[5];
    public Telefono (String p_nroTelefono){
        this.setNroTelefono(p_nroTelefono);
    }
    
    public String delPais(){
        //las llamadas locales no son de interes
        String _pais = "LOCAL";
        
        _pais = this.deArgentina()?_pais="Argentina":null;                    
        return _pais!=  null?_pais:"ERROR";
        
    }    
    /** 
     * remueve todos los caracteres basura del numero de telefono.
     * @return 
     */
    public String soloNumeros (){
        return this.getNroTelefono().replaceAll("[-'`~!@#$%^&*()_|=?;:'\",.<>\\{\\}\\[\\]\\\\\\/ ]", "");
    }
    /**
     * todos los numeros que empiesen +NUmeros entre 0y9  caracter 6 al 14 cualquier numero
     * @return 
     */
    public boolean esValido(){         
        String _regex = "^\\+(?:[0-9] ?){6,14}[0-9]$";
        return this.soloNumeros().matches(_regex);        
    }     
    
    public boolean deArgentina(){
        //String _regex = "^\\+54 0?379[4|5][0-9]+$";
        String _regex = "^\\+54.*";
        if(this.esValido()){ 
            return this.soloNumeros().matches(_regex);
        }else{
            return false;
        }
        
    }
    /**
     * Consideramos la posibilidad de que una llamada se internacional si se da alguna de las condiciones
     * un lumero de una sierta longitud
     * la precencia de un caracter + en el numero.
     * que origen y detino tengan distintosa codigos de area.
     * @return 
     */
    public boolean esInternacional(){                 
        return  this.soloNumeros().length()>=11
                || this.getNroTelefono().indexOf("+")!=-1;
    }

    public String getNroTelefono() {
        return nroTelefono;
    }

    public void setNroTelefono(String nroTelefono) {
        this.nroTelefono = nroTelefono;
    }
}


